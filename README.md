**NOTE:**

The now Blackcoin Legacy client was the original client created by rat4. It is based on the BTC 0.9+ code, and will not be updated. 

After the dust/spam attack that started in late September 2020, we noticed that the Blackcoin original client was causing too many issues,
and now we ask people to migrate to the new Blackcoin More client. 

Since April 2021 the old Blackcoin original client doesn't sync anymore completely. 
**DON'T use this client anymore, you WILL get forked away from the main chain**

Blackcoin More is now our stable client and is based on the newer BTC 0.13+ code. The code will be updated frequently. 
**Please visit https://blackcoinmore.org/ for more info**

The binaries can also be downloaded from here https://gitlab.com/blackcoin/blackcoin-more/-/releases

Visit one of our community support channels if you need help. 

https://blackcoin.org/ for info on the Blackcoin Protocol, Projects, and Support Channels

https://blackcoinmore.org/ for info on the Blackcoin More project.

https://blackcoin.nl/ the most helpful community of blackcoin enthusiasts.

*The Blackcoin Team*
